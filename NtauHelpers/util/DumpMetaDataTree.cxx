#include <NtauHelpers/MuonTPMetaData.h>
#include <NtauHelpers/Utils.h>
#include <TFile.h>
#include <TSystem.h>
#include <TTree.h>

#include <cstring>
#include <iostream>
#include <memory>
#include <string>

bool isSame(const std::vector<unsigned int>& a,
            const std::vector<unsigned int>& b) {
    if (a.size() != b.size())
        return false;
    for (unsigned int i = 0; i < a.size(); ++i) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}
/// Operator for later cross- checks
struct Sorter {
    inline bool operator()(
        const std::shared_ptr<MuonTP::MetaDataStore>& a,
        const std::shared_ptr<MuonTP::MetaDataStore>& b) const {
        if (a->isData() != b->isData())
            return a->isData() < b->isData();
        if (a->runNumber() != b->runNumber())
            return a->runNumber() < b->runNumber();
        if (a->DSID() != b->DSID())
            return a->DSID() < b->DSID();
        return a->LheVariation() < b->LheVariation();
    }
};
using MetaDataSet = std::set<std::shared_ptr<MuonTP::MetaDataStore>, Sorter>;

int main(int argc, char* argv[]) {
    std::vector<std::string> in_files{};
    const std::string APP_NAME{argv[0]};
    std::string out_file{};
    for (int k = 1; k < argc; ++k) {
        std::string current_arg{argv[k]};
        /// Pipe a directory
        if ((current_arg == "--dir" || current_arg == "-d") && k + 1 < argc) {
            do {
                const std::string direct{ResolveFilePath(argv[k + 1])};
                if (direct.empty()) {
                    std::cerr << APP_NAME << " -- " << argv[k + 1]
                              << " is not a directory" << std::endl;
                    return EXIT_FAILURE;
                }
                size_t n_before = in_files.size();
                in_files += ListDirectory(direct, "");
                if (in_files.size() == n_before) {
                    std::cout << APP_NAME << " -- WARNING " << direct
                              << " seems to be empty " << std::endl;
                }
                ++k;
            } while (k + 1 < argc && std::string{argv[k + 1]}[0] != '-');
            /// Pipe the name of the second file or the test file
        } else if (k + 1 < argc &&
                   (current_arg == "--file" || current_arg == "-f")) {
            do {
                const std::string file_path{ResolveFilePath(argv[k + 1])};
                if (file_path.empty()) {
                    std::cerr << APP_NAME << " -- No such file " << argv[k + 1]
                              << std::endl;
                    return EXIT_FAILURE;
                }
                in_files.push_back(file_path);
                ++k;
            } while (k + 1 < argc && std::string{argv[k + 1]}[0] != '-');
        } else if (k + 1 < argc &&
                   (current_arg == "--list" || current_arg == "-l")) {
            const size_t n_before = in_files.size();
            in_files += ReadList(argv[k + 1]);
            if (n_before == in_files.size()) {
                std::cerr << APP_NAME << " -- Could not extract anything from "
                          << argv[k + 1] << std::endl;
                return EXIT_FAILURE;
            }
            ++k;
        } else if (k + 1 < argc &&
                   (current_arg == "--outfile" || current_arg == "-o")) {
            out_file = argv[k + 1];
            ++k;
        } else {
            std::cerr << "Unknown argument " << current_arg << std::endl;
            std::cout << "Usage of " << APP_NAME << std::endl;
            std::cout
                << " --dir / -d <path>, <path>      : Pick up all files in the "
                   "corresponding directories"
                << std::endl;
            std::cout << " --file / -f <path>, <path>     : Add file to merge  "
                      << std::endl;
            std::cout
                << " --list / -l <path>             : Open a list containing "
                   "the file paths. One line per file  "
                << std::endl;
            std::cout
                << " --outfile / -o <path>          : Specify the out file "
                   "name (REQUIRED) "
                << std::endl;
            return EXIT_FAILURE;
        }
    }
    if (out_file.empty()) {
        std::cerr << APP_NAME << " -- no output file was given " << std::endl;
        return EXIT_FAILURE;
    }
    if (in_files.empty()) {
        std::cerr << APP_NAME << " -- no files for merging were given "
                  << std::endl;
        return EXIT_FAILURE;
    }

    MuonTP::NormalizationDataBase* DB =
        MuonTP::NormalizationDataBase::getDataBase();
    if (!DB->init(in_files)) {
        std::cerr << APP_NAME << " Failed to initalize MetaDataDataBase"
                  << std::endl;
        return EXIT_FAILURE;
    }
    DB->PromptMetaDataTree();

    std::shared_ptr<TFile> OutFile = make_out_file(out_file);
    OutFile->cd();
    TTree* MetaDataTree =
        new TTree("MetaDataTree", "MetaData Tree for Small Analysis Ntuples");

    bool isData = DB->isData();
    bool isDerivedAOD{false};
    // Total events
    Long64_t TotalEvents{0}, ProcessedEvents{0};

    // DSID and process ID
    int mcChannelNumber{0};
    unsigned int runNumber{0}, lheId{0};
    double SumW{0}, SumW2{0}, prwLumi{0};
    std::string weightName{};
    // Luminosity information
    std::set<unsigned int> ProcessedBlocks;
    std::set<unsigned int> TotalBlocks;

    std::set<unsigned int>* ProcLumiPtr = &ProcessedBlocks;
    std::set<unsigned int>* TotalLumiPtr = &TotalBlocks;
    MetaDataSet originalStores{};
    // Prepare tree structure
    MetaDataTree->Branch("isData", &isData);
    MetaDataTree->Branch("isDerivedAOD", &isDerivedAOD);
    MetaDataTree->Branch("runNumber", &runNumber);
    MetaDataTree->Branch("TotalEvents", &TotalEvents);
    MetaDataTree->Branch("ProcessedEvents", &ProcessedEvents);
    MetaDataTree->Branch("prwLuminosity", &prwLumi);
    if (!isData) {
        MetaDataTree->Branch("mcChannelNumber", &mcChannelNumber);
        MetaDataTree->Branch("TotalSumW", &SumW);
        MetaDataTree->Branch("TotalSumW2", &SumW2);
        MetaDataTree->Branch("LheId", &lheId);
        MetaDataTree->Branch("LheWeightName", &weightName);
    } else {
        MetaDataTree->Branch("ProcessedLumiBlocks", ProcLumiPtr);
        MetaDataTree->Branch("TotalLumiBlocks", TotalLumiPtr);
    }
    // Write the Monte Carlo  meta-data in its most compact form
    if (!isData) {
        std::vector<unsigned int> DSIDs = DB->GetListOfMCSamples();
        for (unsigned int ds : DSIDs) {
            for (unsigned int lhe = 0; lhe < DB->GetNumLheVariations(ds);
                 ++lhe) {
                // Also the period handlers to distinguis between mc16a/d/f
                std::shared_ptr<MuonTP::MonteCarloPeriodHandler>
                    period_handler = DB->getPeriodHandler(ds, lhe);
                weightName = period_handler->WeightName();
                mcChannelNumber = ds;
                lheId = lhe;
                for (unsigned int period : period_handler->getMCcampaigns()) {
                    std::shared_ptr<MuonTP::MetaDataStore> store =
                        period_handler->getHandler(period);
                    store->Lock();
                    if (!originalStores.insert(store).second) {
                        std::cout
                            << "Something went wrong when caching dsid:" << ds
                            << ", lhe: " << lhe << " period: " << period
                            << std::endl;
                        return EXIT_FAILURE;
                    }
                    runNumber = period;
                    TotalEvents = store->TotalEvents();
                    ProcessedEvents = store->ProcessedEvents();
                    prwLumi = store->prwLuminosity();
                    SumW = store->SumW();
                    SumW2 = store->SumW2();
                    MetaDataTree->Fill();
                }
            }
        }
    } else {
        std::vector<unsigned int> runs = DB->GetRunNumbers();
        for (const auto& run : runs) {
            std::shared_ptr<MuonTP::MetaDataStore> meta =
                DB->getRunMetaData(run);
            meta->Lock();
            if (!meta || !originalStores.insert(meta).second) {
                std::cerr << "Failed to retrieve a meta data element for run "
                          << run << std::endl;
                return EXIT_FAILURE;
            }
            runNumber = run;

            ProcessedBlocks.insert(meta->ProcessedLumiBlocks().begin(),
                                   meta->ProcessedLumiBlocks().end());
            TotalBlocks.insert(meta->TotalLumiBlocks().begin(),
                               meta->TotalLumiBlocks().end());
            TotalEvents = meta->TotalEvents();
            ProcessedEvents = meta->ProcessedEvents();
            MetaDataTree->Fill();
            ProcessedBlocks.clear();
            TotalBlocks.clear();
        }
    }
    OutFile->Write("", TObject::kOverwrite);
    OutFile->Close();
    // Delete the database to load the new file and print it on the screen
    delete MuonTP::NormalizationDataBase::getDataBase();
    DB = MuonTP::NormalizationDataBase::getDataBase();
    if (!DB->init({out_file}))
        return EXIT_FAILURE;
    DB->PromptMetaDataTree();
    if (isData) {
        std::vector<unsigned int> runs = DB->GetRunNumbers();
        for (const unsigned int run : runs) {
            std::shared_ptr<MuonTP::MetaDataStore> meta =
                DB->getRunMetaData(run);
            meta->Lock();
            if (!meta) {
                std::cerr << "Failed to retrieve the MetaDataStore for " << run
                          << std::endl;
                return EXIT_FAILURE;
            }
            MetaDataSet::const_iterator itr = originalStores.find(meta);
            if (itr == originalStores.end()) {
                std::cerr << "The run " << run
                          << " did not made it into the new meta data tree"
                          << std::endl;
                return EXIT_FAILURE;
            }
            /// Check for same event counts
            if ((*itr)->TotalEvents() != meta->TotalEvents() ||
                (*itr)->ProcessedEvents() != meta->ProcessedEvents() ||
                !isSame((*itr)->ProcessedLumiBlocks(),
                        meta->ProcessedLumiBlocks()) ||
                !isSame((*itr)->TotalLumiBlocks(), meta->TotalLumiBlocks())) {
                std::cerr << "Different event counts found between original "
                             "database and the skimmed one "
                          << (*itr)->TotalEvents() << " vs. "
                          << meta->TotalEvents()
                          << ", processed: " << (*itr)->ProcessedEvents()
                          << " vs. " << meta->ProcessedEvents() << std::endl
                          << (*itr)->ProcessedLumiBlocks() << " vs. "
                          << meta->ProcessedLumiBlocks() << std::endl
                          << (*itr)->TotalLumiBlocks() << " vs. "
                          << meta->TotalLumiBlocks() << std::endl;
                return EXIT_FAILURE;
            }
        }
    } else {
        std::vector<unsigned int> DSIDs = DB->GetListOfMCSamples();
        for (unsigned int ds : DSIDs) {
            for (unsigned int lhe = 0; lhe < DB->GetNumLheVariations(ds);
                 ++lhe) {
                // Also the period handlers to distinguis between mc16a/d/f
                std::shared_ptr<MuonTP::MonteCarloPeriodHandler>
                    period_handler = DB->getPeriodHandler(ds, lhe);
                for (unsigned int period : period_handler->getMCcampaigns()) {
                    std::shared_ptr<MuonTP::MetaDataStore> meta =
                        period_handler->getHandler(period);
                    meta->Lock();
                    MetaDataSet::const_iterator itr = originalStores.find(meta);
                    if (itr == originalStores.end()) {
                        std::cerr
                            << "The dsid: " << ds << ", lhe:" << lhe
                            << " did not made it into the new meta data tree"
                            << std::endl;
                        return EXIT_FAILURE;
                    }
                    if ((*itr)->TotalEvents() != meta->TotalEvents() ||
                        (*itr)->ProcessedEvents() != meta->ProcessedEvents() ||
                        std::abs((*itr)->SumW() - meta->SumW()) >
                            std::numeric_limits<float>::epsilon() ||
                        std::abs((*itr)->SumW2() - meta->SumW2()) >
                            std::numeric_limits<float>::epsilon()) {
                        std::cerr << "Inconsistent meta data for " << ds << ","
                                  << lhe << " found." << std::endl;
                        return EXIT_FAILURE;
                    }
                }
            }
        }
    }
    std::cout << "Successfully dumped a new meta data tree into " << out_file
              << std::endl;
    return EXIT_SUCCESS;
}
