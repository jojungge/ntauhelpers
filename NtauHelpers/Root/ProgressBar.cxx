#include <NtauHelpers/ProgressBar.h>
ProgressBar::~ProgressBar() {
    finalize();
}
void ProgressBar::start_counter(Long64_t n_entries, PromptInterval interval) {
    m_watch.Start(true);
    m_is_started = true;
    m_entries = n_entries;
    switch (interval) {
        case PromptInterval::each_0p5percent:
            m_prompt_interval = 0.005 * m_entries;
            break;
        case PromptInterval::each_1percent:
            m_prompt_interval = 0.01 * m_entries;
            break;
        case PromptInterval::each_2percent:
            m_prompt_interval = 0.02 * m_entries;
            break;
        case PromptInterval::each_5percent:
            m_prompt_interval = 0.05 * m_entries;
            break;
        case PromptInterval::each_10percent:
            m_prompt_interval = 0.10 * m_entries;
            break;
        case PromptInterval::each_25percent:
            m_prompt_interval = 0.25 * m_entries;
            break;
        case PromptInterval::each_50percent:
            m_prompt_interval = 0.5 * m_entries;
            break;
        /// Fixed ranges
        case PromptInterval::each_count:
            m_prompt_interval = 1;
            break;
        case PromptInterval::each_10count:
            m_prompt_interval = 10;
            break;
        case PromptInterval::each_100count:
            m_prompt_interval = 100;
            break;
        case PromptInterval::each_1000count:
            m_prompt_interval = 1000;
            break;
        case PromptInterval::each_10000count:
            m_prompt_interval = 10000;
            break;
        case PromptInterval::each_100000count:
            m_prompt_interval = 100000;
            break;
    }
    if (m_prompt_interval == 0) {
        std::cout << "ProgressBar -- The number of entries " << m_entries
                  << " is too small for the chosen interval. Will prompt every "
                     "update "
                  << std::endl;
        m_prompt_interval = 1;
    } else if (m_prompt_interval >= m_entries) {
        std::cout
            << "ProgressBar -- The number of entries " << m_entries
            << " is too small for the chosen interval. Will swtich off prompt "
            << std::endl;
        m_prompt_interval = m_entries;
    }
}
void ProgressBar::increment_counter() {
    update_counter(1);
}

void ProgressBar::update_counter(Long64_t n_entries) {
    std::lock_guard<std::mutex> guard{m_mutex};

    if (!m_is_started) {
        throw std::runtime_error("The progress bar is not yet started");
    }
    const Long64_t mod_before = m_proc_entries % m_prompt_interval;
    m_proc_entries += n_entries;
    const Long64_t mod_after = m_proc_entries % m_prompt_interval;
    if (n_entries < m_prompt_interval && (mod_after > mod_before))
        return;
    const double real_time = m_watch.RealTime();
    const double cpu_time = m_watch.CpuTime();
    m_watch.Continue();

    const double progress = (1. * m_proc_entries) / m_entries;

    std::cout << '\r' << "Entry " << num_evt_to_screen(m_proc_entries) << " / "
              << num_evt_to_screen(m_entries) << " ( " << std::setprecision(2)
              << (progress * 100.) << " %) " << std::setprecision(6);
    std::cout << " after " << TimeHMS(real_time) << " (" << TimeHMS(cpu_time)
              << " CPU). Rate: " << std::setprecision(3)
              << print_speed(real_time) << "  (" << print_speed(cpu_time)
              << " CPU)  E.T.A.: " << TimeHMS(real_time * (1. / progress - 1.))
              << "               ";
    std::cout << '\r' << std::flush;
}
void ProgressBar::finalize() {
    if (!m_is_started)
        return;
    const double real_time = m_watch.RealTime();
    const double cpu_time = m_watch.CpuTime();
    m_watch.Stop();
    m_is_started = false;

    std::cout << std::endl
              << std::endl
              << '\r' << "Finished processing of "
              << num_evt_to_screen(m_proc_entries) << " entries in "
              << TimeHMS(real_time) << " (" << TimeHMS(cpu_time)
              << " CPU). Achieved rate " << print_speed(real_time) << "  ("
              << print_speed(cpu_time) << " CPU)" << std::endl
              << std::endl;
}
std::string ProgressBar::num_evt_to_screen(const Long64_t n) const {
    int log10 = std::log10(n);
    double n_dbl = n;

    static const std::array<std::string, 5> units{"", "k", "M", "G", "T"};
    int unit_idx = 0;
    while ((log10 > 3 || n_dbl > 599) && unit_idx < 5) {
        log10 -= 3;
        n_dbl /= 1000;
        ++unit_idx;
    }
    std::stringstream sstr;
    sstr << std::setprecision(3) << n_dbl << " " << units[unit_idx];
    return sstr.str();
}
std::string ProgressBar::print_speed(const double time) const {
    /// Natural time interval are miliseconds
    double speed = m_proc_entries / time;
    static const std::array<std::string, 6> units{"n", "m", "", "k", "M", "G"};

    /// Decide about the frequency
    int log10 = std::log10(speed);
    int unit_idx = 2;
    if (log10 < 0) {
        while ((log10 < 0 || speed < 100) && unit_idx > 0) {
            --unit_idx;
            speed *= 1000;
            log10 += 3;
        }
    } else {
        while ((log10 > 3 || speed > 599) && unit_idx < 6) {
            log10 -= 3;
            speed /= 1000;
            ++unit_idx;
        }
    }
    std::stringstream sstr;
    sstr << std::setprecision(2) << speed << " " << units[unit_idx] << "Hz";
    return sstr.str();
}
