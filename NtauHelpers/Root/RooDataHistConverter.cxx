#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/RooDataHistConverter.h>

#define WARN_DIMENSION(N)                                                 \
    if (dimension() < N) {                                                \
        std::cout << __FILE__ << ":" << __LINE__                          \
                  << " -- WARNING: Invalid dimension " << N               \
                  << " requested. The template" << name() << " only has " \
                  << dimension() << " dimensions. " << std::endl;         \
    }

RooDataHistConverter::RooDataHistConverter(std::shared_ptr<TH1> histo)
    : m_histo{histo} {
    initialize(histo->GetName());
}
std::shared_ptr<TH1> RooDataHistConverter::get_histo() const {
    return m_histo;
}
unsigned int RooDataHistConverter::dimension() const {
    return m_histo->GetDimension();
}
double RooDataHistConverter::x_low() const {
    return get_histo()->GetXaxis()->GetBinLowEdge(1);
}
double RooDataHistConverter::x_high() const {
    return get_histo()->GetXaxis()->GetBinUpEdge(nBinsX());
}
unsigned int RooDataHistConverter::nBinsX() const {
    return get_histo()->GetNbinsX();
}
double RooDataHistConverter::y_low() const {
    WARN_DIMENSION(2);
    return get_histo()->GetYaxis()->GetBinLowEdge(1);
}
double RooDataHistConverter::y_high() const {
    WARN_DIMENSION(2);
    return get_histo()->GetYaxis()->GetBinUpEdge(nBinsY());
}
unsigned int RooDataHistConverter::nBinsY() const {
    WARN_DIMENSION(2);
    return get_histo()->GetNbinsY();
}
double RooDataHistConverter::z_low() const {
    WARN_DIMENSION(3);
    return get_histo()->GetZaxis()->GetBinLowEdge(1);
}
double RooDataHistConverter::z_high() const {
    WARN_DIMENSION(3);
    return get_histo()->GetZaxis()->GetBinUpEdge(nBinsY());
}
unsigned int RooDataHistConverter::nBinsZ() const {
    WARN_DIMENSION(3);
    return get_histo()->GetNbinsZ();
}
std::string RooDataHistConverter::name() const {
    return m_template_name;
}
std::shared_ptr<RooDataHist> RooDataHistConverter::get() const {
    return m_roo_hist;
}
std::shared_ptr<RooRealVar> RooDataHistConverter::x_spectrum() const {
    return m_Roo_x_var;
}
std::shared_ptr<RooRealVar> RooDataHistConverter::y_spectrum() const {
    WARN_DIMENSION(2);
    return m_Roo_y_var;
}
std::shared_ptr<RooRealVar> RooDataHistConverter::z_spectrum() const {
    WARN_DIMENSION(3);
    return m_Roo_z_var;
}
void RooDataHistConverter::initialize(const std::string& histo_name) {
    if (histo_name.empty()) {
        throw std::runtime_error("No histogram name is given");
    }
    m_template_name = histo_name;
    m_Roo_x_var = std::make_shared<RooRealVar>(
        ("Xvariable__" + histo_name).c_str(), m_histo->GetXaxis()->GetTitle(),
        x_low(), x_high());
    RooArgSet arg_set{*m_Roo_x_var};
    if (dimension() > 1) {
        m_Roo_y_var = std::make_shared<RooRealVar>(
            ("Yvariable__" + histo_name).c_str(),
            m_histo->GetYaxis()->GetTitle(), y_low(), y_high());
        arg_set.add(*m_Roo_y_var);
    }
    if (dimension() > 2) {
        m_Roo_z_var = std::make_shared<RooRealVar>(
            ("Zvariable__" + histo_name).c_str(),
            m_histo->GetZaxis()->GetTitle(), z_low(), z_high());
        arg_set.add(*m_Roo_z_var);
    }
    m_roo_hist = std::make_shared<RooDataHist>(
        (histo_name).c_str(), "data", arg_set, RooFit::Import(*get_histo()));
}
void RooDataHistConverter::set_name(const std::string& histo_name) {
    std::cout << "RooDataHistConverter --- INFO: Rename " << name() << " to "
              << histo_name << std::endl;
    initialize(histo_name);
}
#undef WARN_DIMENSION
