#ifndef NTAUHELPERS_PLOTPSTPROCESSOR_IXX
#define NTAUHELPERS_PLOTPSTPROCESSOR_IXX
#include "NtauHelpers/HistoUtils.h"
#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h"
#include "NtupleAnalysisUtils/Plot/PostProcessingPopulators.h"

#define STANDARD_CTORS(POST_PROC)                                     \
    template <class HistoType>                                        \
    POST_PROC<HistoType>::POST_PROC(const Plot<HistoType>& inputPlot) \
        : m_input{inputPlot.getPopulator()} {}                        \
    template <class HistoType>                                        \
    POST_PROC<HistoType>::POST_PROC(                                  \
        const IPopulatorSource<HistoType>& inputSource)               \
        : m_input{inputSource.clonePopulator()} {}                    \
    template <class HistoType>                                        \
    std::shared_ptr<IPlotPopulator<HistoType>>                        \
    POST_PROC<HistoType>::clonePopulator() const {                    \
        return std::make_shared<POST_PROC>(*this);                    \
    }

#define STANDARD_PAIR_CTORS(POST_PROC)                                 \
    template <class HistoType>                                         \
    POST_PROC<HistoType>::POST_PROC(const Plot<HistoType>& firstPlot,  \
                                    const Plot<HistoType>& secondPlot) \
        : m_first{firstPlot.getPopulator()},                           \
          m_second{secondPlot.getPopulator()} {}                       \
    template <class HistoType>                                         \
    POST_PROC<HistoType>::POST_PROC(                                   \
        const IPopulatorSource<HistoType>& firstSource,                \
        const IPopulatorSource<HistoType>& secondSource)               \
        : m_first{firstSource.clonePopulator()},                       \
          m_second{secondSource.clonePopulator()} {}                   \
    template <class HistoType>                                         \
    std::shared_ptr<IPlotPopulator<HistoType>>                         \
    POST_PROC<HistoType>::clonePopulator() const {                     \
        return std::make_shared<POST_PROC>(*this);                     \
    }

/// #####################################################
///                  OverFlowPuller
/// #####################################################
STANDARD_CTORS(OverFlowPuller)
template <class HistoType>
std::shared_ptr<HistoType> OverFlowPuller<HistoType>::populate() {
    std::shared_ptr<HistoType> histo = std::dynamic_pointer_cast<HistoType>(
        to_TH1(clone(m_input->populate())));
    PlotUtils::shiftOverflows(histo.get());
    return histo;
}
/// ######################################################
///              RmsCalculator
/// ######################################################
///  Takes a 2D or 3D histogram and projects the RMS into N-1 dimensions. The
///  axis used for the calculation of the RMS is the z-axis and y-axis for the
///  3D and 2D histograms
STANDARD_CTORS(RmsCalculator)
template <class HistoType>
std::shared_ptr<HistoType> RmsCalculator<HistoType>::populate() {
    /// Retrieve first the histogram
    std::shared_ptr<HistoType> histo = m_input->populate();
    /// Do nothing if the user did not undersand the concept of the RMS
    if (histo->GetDimension() == 1) {
        return histo;
    }
    std::shared_ptr<HistoType> output_histo;
    /// 2D histogram
    if (histo->GetDimension() == 2) {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            double rms{0.}, num_bins{0.};
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double content = histo->GetBinContent(bin_x, bin_y);
                if (std::abs(content) <= DBL_EPSILON)
                    continue;
                num_bins += content;
                rms += std::pow(
                    content * histo->GetYaxis()->GetBinCenter(bin_y), 2);
            }
            if (!num_bins)
                continue;
            rms = std::sqrt(rms / num_bins);
            output_histo->SetBinContent(bin_x, rms);
        }
    } else {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()),
                               ExtractBinning(histo->GetYaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double rms{0.}, num_bins{0.};
                for (int bin_z = 0; bin_z <= histo->GetNbinsZ(); ++bin_z) {
                    double content = histo->GetBinContent(bin_x, bin_y, bin_z);
                    if (std::abs(content) <= DBL_EPSILON)
                        continue;
                    num_bins += content;
                    rms += std::pow(
                        content * histo->GetZaxis()->GetBinCenter(bin_z), 2);
                }
                if (!num_bins)
                    continue;
                rms = std::sqrt(rms / num_bins);
                output_histo->SetBinContent(bin_x, bin_y, rms);
            }
        }
    }
    copy_styling(histo, output_histo);
    return output_histo;
}

/// ######################################################
///              MeanCalculator
/// ######################################################
STANDARD_CTORS(MeanCalculator)
template <class HistoType>
std::shared_ptr<HistoType> MeanCalculator<HistoType>::populate() {
    std::shared_ptr<HistoType> histo = m_input->populate();
    if (histo->GetDimension() == 1)
        return histo;
    std::shared_ptr<HistoType> output_histo;
    ///
    if (histo->GetDimension() == 2) {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            double rms{0.}, num_bins{0.};
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double content = histo->GetBinContent(bin_x, bin_y);
                if (std::abs(content) <= DBL_EPSILON)
                    continue;
                num_bins += content;
                rms += content * histo->GetYaxis()->GetBinCenter(bin_y);
            }
            if (!num_bins)
                continue;
            rms = rms / num_bins;
            output_histo->SetBinContent(bin_x, rms);
        }
    } else {
        output_histo = MakeTH1(ExtractBinning(histo->GetXaxis()),
                               ExtractBinning(histo->GetYaxis()));
        for (int bin_x = 0; bin_x <= histo->GetNbinsX(); ++bin_x) {
            for (int bin_y = 0; bin_y <= histo->GetNbinsY(); ++bin_y) {
                double rms{0.}, num_bins{0.};
                for (int bin_z = 0; bin_z <= histo->GetNbinsZ(); ++bin_z) {
                    double content = histo->GetBinContent(bin_x, bin_y, bin_z);
                    if (std::abs(content) <= DBL_EPSILON)
                        continue;
                    num_bins += content;
                    rms += content * histo->GetZaxis()->GetBinCenter(bin_z);
                }
                if (!num_bins)
                    continue;
                rms = rms / num_bins;
                output_histo->SetBinContent(bin_x, bin_y, rms);
            }
        }
    }
    copy_styling(histo, output_histo);
    return output_histo;
}
/// ################################################################
///              SqrtPlot
/// ################################################################
STANDARD_CTORS(SqrtPlot)
template <class HistoType>
std::shared_ptr<HistoType> SqrtPlot<HistoType>::populate() {
    std::shared_ptr<HistoType> histo = to_TH1(m_input->populate());
    if (!histo)
        throw std::runtime_error("No histogram given to SqrtPlot");
    std::shared_ptr<HistoType> output_histo = clone(histo);
    for (unsigned int bin = 0; bin < PlotUtils::getNbins(histo.get()); ++bin) {
        output_histo->SetBinContent(
            bin, sign(histo->GetBinContent(bin)) *
                     std::sqrt(std::abs(histo->GetBinContent(bin))));
        output_histo->SetBinError(bin, std::abs(histo->GetBinContent(bin)));
    }
    return output_histo;
}
/// ################################################################
///              ProductPlot
/// ################################################################
STANDARD_PAIR_CTORS(ProductPlot)
template <class HistoType>
std::shared_ptr<HistoType> ProductPlot<HistoType>::populate() {
    std::shared_ptr<HistoType> first_histo = to_TH1(m_first->populate());
    std::shared_ptr<HistoType> second_histo = to_TH1(m_second->populate());
    if (!first_histo || !second_histo)
        throw std::runtime_error("No histogram given to ProductPlot");
    std::shared_ptr<HistoType> output_histo = clone(first_histo);
    if (!same_binning(first_histo, second_histo)) {
        output_histo->Reset();
    } else {
        output_histo->Multiply(second_histo.get());
    }
    return output_histo;
}

#undef STANDARD_CTORS
#undef STANDARD_PAIR_CTORS
#endif