#ifndef NTAUHELPERS_HISTOBINCONFIG_H
#define NTAUHELPERS_HISTOBINCONFIG_H

#include <NtupleAnalysisUtils/Configuration/UserSetting.h>
#include <TH1.h>

#include <memory>
/// Helper class to create a histogram with any kind of binning style etc

#define HISTO_OPTS(HISTO_TYPE)                                                 \
    /* Define the name of the histogram */                                     \
    UserSetting<std::string, HISTO_TYPE> name{"", this};                       \
    /* Histogram title */                                                      \
    UserSetting<std::string, HISTO_TYPE> title{"", this};                      \
    /* Define the title of the x-axis*/                                        \
    UserSetting<std::string, HISTO_TYPE> x_title{"", this};                    \
    /* Define the title of the y-axis  */                                      \
    UserSetting<std::string, HISTO_TYPE> y_title{"", this};                    \
    /* Define the title of the z-axis */                                       \
    UserSetting<std::string, HISTO_TYPE> z_title{"", this};                    \
    /*Pipe the bin edges of the x-axis */                                      \
    UserSetting<std::vector<double>, HISTO_TYPE> x_bins{{}, this};             \
    /* If the bins are not explicitly given, then the following three settings \
       have to be set by the user */                                           \
                                                                               \
    /* Define the lower edge of the x-axis */                                  \
    UserSetting<double, HISTO_TYPE> x_min{-DBL_MAX, this};                     \
    /* Define the upper edge of the x-axis */                                  \
    UserSetting<double, HISTO_TYPE> x_max{DBL_MAX, this};                      \
    /*Define the segmentation */                                               \
    UserSetting<unsigned int, HISTO_TYPE> x_num_bins{0, this};                 \
    /*If the bins are configured via x_min, x_max, the user can define them to \
     * be separated in a log scale */                                          \
    UserSetting<bool, HISTO_TYPE> x_log_binning{false, this};                  \
    /* Bin labels of the x-axis */                                             \
    UserSetting<std::map<int, std::string>, HISTO_TYPE> x_bin_labels{{},       \
                                                                     this};    \
                                                                               \
    UserSetting<std::vector<double>, HISTO_TYPE> y_bins{{}, this};             \
    /* If the bins are not explicitly given, then the following three settings \
       have to be set by the user  */                                          \
    /* Define the lower edge of the y-axis */                                  \
    UserSetting<double, HISTO_TYPE> y_min{-DBL_MAX, this};                     \
    /* Define the upper edge of the y-axis */                                  \
    UserSetting<double, HISTO_TYPE> y_max{DBL_MAX, this};                      \
    /*Define the segmentation */                                               \
    UserSetting<unsigned int, HISTO_TYPE> y_num_bins{0, this};                 \
    /* If the bins are configured via y_min, y_max, the user can define them   \
     * to be separated in a log scale */                                       \
    UserSetting<bool, HISTO_TYPE> y_log_binning{false, this};                  \
    /* Bin labels of the y-axis */                                             \
    UserSetting<std::map<int, std::string>, HISTO_TYPE> y_bin_labels{{},       \
                                                                     this};    \
                                                                               \
    UserSetting<std::vector<double>, HISTO_TYPE> z_bins{{}, this};             \
    /* If the bins are not explicitly given, then the following three settings \
       have to be set by the user */                                           \
    /* Define the lower edge of the z-axis */                                  \
    UserSetting<double, HISTO_TYPE> z_min{-DBL_MAX, this};                     \
    /* Define the upper edge of the z-axis */                                  \
    UserSetting<double, HISTO_TYPE> z_max{DBL_MAX, this};                      \
    /* Define the segmentation */                                              \
    UserSetting<unsigned int, HISTO_TYPE> z_num_bins{0, this};                 \
    /* If the bins are configured via z_min, z_max, the user can define them   \
     * to be separated in a log scale */                                       \
    UserSetting<bool, HISTO_TYPE> z_log_binning{false, this};                  \
    /* Bin axis labels of the z-axis */                                        \
    UserSetting<std::map<int, std::string>, HISTO_TYPE> z_bin_labels{{}, this};

namespace PlotUtils {

class HistoBinConfig {
   public:
    /// Standard constructors
    HistoBinConfig() = default;
    HistoBinConfig(const HistoBinConfig&);
    HistoBinConfig& operator=(const HistoBinConfig&);

    HistoBinConfig(HistoBinConfig&&) = default;

    HISTO_OPTS(HistoBinConfig)

    /// Crates a histogram from the given settings
    std::shared_ptr<TH1> make_histo();

   private:
    void finalize_settings();
    void set_styling(std::shared_ptr<TH1> histo);
};

class ProfileBinConfig {
   public:
    ProfileBinConfig() = default;
    ProfileBinConfig(const ProfileBinConfig&);
    ProfileBinConfig& operator=(const ProfileBinConfig&);
    ProfileBinConfig(ProfileBinConfig&&) = default;

    HISTO_OPTS(ProfileBinConfig)

    /// Profile value has to be at least greater than
    UserSetting<double, ProfileBinConfig> min_range{-DBL_MAX, this};
    /// Profile value has to be less than
    UserSetting<double, ProfileBinConfig> max_range{DBL_MAX, this};
    /// Error option "s", "i", "g" (cf.
    /// https://root.cern.ch/doc/master/classTProfile.html#a1ff9340284c73ce8762ab6e7dc0e6725)
    UserSetting<std::string, ProfileBinConfig> error_option{"", this};

    std::shared_ptr<TH1> make_histo();

   private:
    void finalize_settings();
    void set_styling(std::shared_ptr<TH1> histo);
};

}  // namespace PlotUtils
#undef HISTO_OPTS
#endif
