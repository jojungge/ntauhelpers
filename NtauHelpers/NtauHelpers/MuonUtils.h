#ifndef NTAUHELPERS_MUONUTILS_H
#define NTAUHELPERS_MUONUTILS_H

// #include <MuonStationIndex/MuonStationIndex.h>
#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>
#include <xAODMuon/Muon.h>

std::string to_string(xAOD::Muon::Quality WP);
std::string to_string(xAOD::Muon::TrackParticleType TP);
std::string to_string(xAOD::Muon::Author A);

/// Helper service to ease the identifier parsing in the offline code
#ifndef XAOD_ANALYSIS
#include <MuonIdHelpers/MdtIdHelper.h>
#endif
class IdDictParser;
class MdtIdHelper;

class IdentifierService final {
   public:
    static IdentifierService* getService(
        const std::string& muon_dict =
            "IdDictParser/IdDictMuonSpectrometer_R.03.xml");

    ~IdentifierService();

    const MdtIdHelper& mdtHelper() const;

   private:
    /// Singleton pattern
    IdentifierService(const std::string& muon_dict);
    IdentifierService(const IdentifierService&) = delete;
    void operator=(const IdentifierService&) = delete;

    static IdentifierService* m_inst;
#ifndef XAOD_ANALYSIS
    std::unique_ptr<IdDictParser> m_dict_parser;
    std::unique_ptr<MdtIdHelper> m_mdt_helper;
#endif
};

#endif
