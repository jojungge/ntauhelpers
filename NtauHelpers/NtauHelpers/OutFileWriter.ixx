#ifndef NTAUHELPERS_OUTFILEWRITER_IXX
#define NTAUHELPERS_OUTFILEWRITER_IXX

#include <NtauHelpers/OutFileWriter.h>
#include <NtauHelpers/ProgressBar.h>
#include <NtauHelpers/Utils.h>

#include <mutex>
/// ###################################################################
///      Simple helper class which
///      takes care that Plots are written
///      to the TFile. Any kind of Plot is supported
///     The plots are automatically written as soon as
///     the object is destoryed. Plots with duplicate file-names
///
/// #############################################
namespace {
std::mutex destruct_mutex_plot_writer;
}

template <class T>
template <class O>
PlotWriter<T>::PlotWriter(const PlotWriter<O>& other)
    : PlotWriter(other.get()) {}
template <class T>
PlotWriter<T>::PlotWriter(const std::shared_ptr<TFile>& out_file)
    : m_out_file{out_file}, m_to_write{} {}
/// Alternative constructor using the string_path
template <class T>
PlotWriter<T>::PlotWriter(const std::string& out_path)
    : PlotWriter(make_out_file(out_path)) {}

template <class T>
std::shared_ptr<TFile> PlotWriter<T>::get() const {
    return m_out_file;
}
template <class T>
bool PlotWriter<T>::add_plot(const std::shared_ptr<T>& pl,
                             const std::string& save_as) {
    return add_plot(Plot<T>(CopyExisting(pl)), save_as);
}

template <class T>
bool PlotWriter<T>::add_plot(const Plot<T>& pl, const std::string& save_as) {
    if (!m_out_file) {
        std::cerr << "Invalid TFile pointer was given" << std::endl;
        return false;
    }
    if (save_as.empty()) {
        std::cerr << "PlotWriter::add_plot() -- Empty file name given "
                  << std::endl;
        return false;
    }
    if (save_as.rfind("/") == save_as.size() - 1) {
        std::cerr
            << "PlotWritter::add_plot() -- Only the directory is given but "
               "where is the name in  "
            << save_as << "?" << std::endl;
        return false;
    }
    if (m_to_write.find(save_as) != m_to_write.end()) {
        std::cerr << "There already exists plot to be written " << save_as
                  << std::endl;
        return false;
    }
    m_to_write.insert(std::make_pair(save_as, pl));
    return true;
}

template <class T>
void PlotWriter<T>::write() {
    if (!m_out_file || m_to_write.empty())
        return;
    const std::lock_guard<std::mutex> lock(destruct_mutex_plot_writer);
    ProgressBar progress;
    std::cout << "Closing file: " << m_out_file->GetName() << " will dump "
              << m_to_write.size() << " objects to it" << std::endl;
    progress.start_counter(m_to_write.size(),
                           ProgressBar::PromptInterval::each_0p5percent);

    std::string last_dir{};
    TDirectory* write_dir{nullptr};
    /// It should be a std::pair<std::string,Plot<T>>
    for (auto& obj : m_to_write) {
        /// Define the final directory
        const std::string dir_name = obj.first.substr(0, obj.first.rfind("/"));
        if (dir_name != last_dir) {
            write_dir = PlotUtils::mkdir(obj.first, m_out_file.get());
            last_dir = dir_name;
        }
        std::string obj_name =
            obj.first.substr(obj.first.rfind("/") + 1, std::string::npos);
        Plot<T> plt{std::move(obj.second)};
        plt.populate(false);  /// Avoid applying the application of the
                              /// PlotFormat as this becomes slow
        std::shared_ptr<T> histo_ptr = plt.getSharedHisto();
        if (!histo_ptr) {
            std::cout << "Something went Horrible wrong when making this one "
                      << obj.first << std::endl;
            progress.increment_counter();
            continue;
        }
        write_dir->cd();
        histo_ptr->Write(obj_name.c_str());
        progress.increment_counter();
    }
    m_to_write.clear();
}
template <class T>
PlotWriter<T>::~PlotWriter() {
    write();
}
#endif
