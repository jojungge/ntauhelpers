#ifndef NTAUHELPERS_CONTAINERFILLER_H
#define NTAUHELPERS_CONTAINERFILLER_H
#include <NtauHelpers/ContainerSelection.h>
#include <NtauHelpers/HistoBinConfig.h>
/// Helper class to create
template <class IFillable, class ProcessThis>
class ContainerFiller : public ISortableByID {
   public:
    /// Standard constructor with the minimal requirement to provide the
    /// selection
    ///  size_function  -> Function to return the container size
    ///  sel_function   -> Selection function to be applied on each element of
    ///  the object collection
    /// Unique name of the selection use for the saving of the histograms into
    /// PDFs or TFiles Title: Title plotted on a canvas

    using fill_func = std::function<void(IFillable*, ProcessThis&, size_t)>;
    ContainerFiller(fill_func filler, std::shared_ptr<IFillable> template_ptr);

    const fill_func& get_filler() const { return m_func; }

    std::shared_ptr<IFillable> get() const { return m_ptr; }

    std::string name() const { return m_ptr->GetName(); }

    PlotFillInstructionWithRef<IFillable, ProcessThis> make_instruction(
        const ContainerSelection<ProcessThis>& selection) const;

   private:
    std::function<void(IFillable*, ProcessThis&, size_t)> m_func;
    std::shared_ptr<IFillable> m_ptr{nullptr};
};

#include <NtauHelpers/ContainerFiller.ixx>
#endif
