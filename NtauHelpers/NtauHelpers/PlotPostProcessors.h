#ifndef NTAUHELPERS_PLOTPSTPROCESSOR_H
#define NTAUHELPERS_PLOTPSTPROCESSOR_H
#include "NtauHelpers/HistoUtils.h"
#include "NtupleAnalysisUtils/Helpers/HistoManipulation.h"
#include "NtupleAnalysisUtils/Plot/PostProcessingPopulators.h"

/// Pulls the overflows into the visible range
template <class HistoType>
class OverFlowPuller : public IPlotPopulator<HistoType> {
   public:
    /// inputSource can be either a Plot or another populator.
    OverFlowPuller(const Plot<HistoType>& inputPlot);
    OverFlowPuller(const IPopulatorSource<HistoType>& inputSource);
    OverFlowPuller(const OverFlowPuller& other) = default;

    std::shared_ptr<HistoType> populate() override;
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const;

   private:
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
};

/// Calculates the sqrt of each histogram bin content
template <class HistoType>
class SqrtPlot : public IPlotPopulator<HistoType> {
   public:
    SqrtPlot(const Plot<HistoType>& inputPlot);
    SqrtPlot(const IPopulatorSource<HistoType>& inputSource);

    std::shared_ptr<HistoType> populate() override;
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const;

   private:
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
};

template <class HistoType>
class ProductPlot : public IPlotPopulator<HistoType> {
   public:
    ProductPlot(const Plot<HistoType>& first, const Plot<HistoType>& second);
    ProductPlot(const IPopulatorSource<HistoType>& first,
                const IPopulatorSource<HistoType>& second);

    std::shared_ptr<HistoType> populate() override;
    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const;

   private:
    std::shared_ptr<IPlotPopulator<HistoType>> m_first;
    std::shared_ptr<IPlotPopulator<HistoType>> m_second;
};

/// Takes a 2D or 3D histogram and projects the RMS into N-1 dimensions. The
/// axis used for the calculation of the RMS is the z-axis and y-axis for the 3D
/// and 2D histograms
template <class HistoType>
class RmsCalculator : public IPlotPopulator<HistoType> {
   public:
    RmsCalculator(const IPopulatorSource<HistoType>& plot);
    RmsCalculator(const Plot<HistoType>& plot);
    RmsCalculator(const RmsCalculator& other) = default;

    std::shared_ptr<HistoType> populate() override;

    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const;

   private:
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
};
///// Mean post processor

template <class HistoType>
class MeanCalculator : public IPlotPopulator<HistoType> {
   public:
    MeanCalculator(const IPopulatorSource<HistoType>& plot);
    MeanCalculator(const Plot<HistoType>& plot);
    MeanCalculator(const MeanCalculator& other) = default;

    std::shared_ptr<HistoType> populate() override;

    std::shared_ptr<IPlotPopulator<HistoType>> clonePopulator() const;

   private:
    std::shared_ptr<IPlotPopulator<HistoType>> m_input;
};

#include <NtauHelpers/PlotPostProcessors.ixx>
#endif