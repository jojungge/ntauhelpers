#ifndef NTAUHELPERS_PROGRESSBAR_H
#define NTAUHELPERS_PROGRESSBAR_H
#include <NtauHelpers/Utils.h>
#include <TStopwatch.h>

/// Simple helper class to illustrate the progress of a job
class ProgressBar {
   public:
    /// Standard constructor
    ProgressBar() = default;
    ~ProgressBar();

    /// Options to set the progress frequency
    enum class PromptInterval {
        each_0p5percent,
        each_1percent,
        each_2percent,
        each_5percent,
        each_10percent,
        each_25percent,
        each_50percent,

        each_count,
        each_10count,
        each_100count,
        each_1000count,
        each_10000count,
        each_100000count
    };

    /// Start of the progrss bar
    ///     -- Expected number of events
    ///     --- Prompt interval settings
    void start_counter(Long64_t n_entries, PromptInterval interval);

    /// Increments the conter by 1 unit
    void increment_counter();

    /// Increments the counter by any unit
    void update_counter(Long64_t n_entries);

    /// Stops the counter and prompts the final statistics
    void finalize();

   private:
    TStopwatch m_watch{};
    std::mutex m_mutex{};
    bool m_is_started{false};
    Long64_t m_entries{0};
    Long64_t m_prompt_interval{0};
    Long64_t m_proc_entries{0};

    std::string num_evt_to_screen(const Long64_t n) const;
    std::string print_speed(const double time) const;
};

#endif