#ifndef NTAUHELPERS__ROODATAHISTCONVERTER_H
#define NTAUHELPERS__ROODATAHISTCONVERTER_H
/// The RooDataHistConverter converts a TH1 histogram to a RooDataHist useable
/// for  RooFit routines later
#include <NtauHelpers/Utils.h>
#include <RooAbsPdf.h>
#include <RooDataHist.h>
#include <RooDataSet.h>
#include <RooRealVar.h>

class RooDataHistConverter {
   public:
    /// Standard constructor taking a TH1 variable
    RooDataHistConverter(std::shared_ptr<TH1> histo);

    /// Returns the pointer to the histogram
    std::shared_ptr<TH1> get_histo() const;
    /// Returns the dimension of the histogram
    unsigned int dimension() const;

    /// Returns the lower edge of the first bin along the x-axis
    double x_low() const;
    /// Returns the upper edge of the last bin along the x-axis
    double x_high() const;
    /// Returns the number of bins along the Xaxis
    unsigned int nBinsX() const;

    /// Returns the lower edge of the first bin along the y-axis (If dimension
    /// is 2)
    double y_low() const;
    /// Returns the upper edge of the last bin along the y-axis
    double y_high() const;
    /// Returns the number of bins along the Xaxis
    unsigned int nBinsY() const;

    /// Returns the lower edge of the first bin along the z-axis
    double z_low() const;
    /// Returns the upper edge of the last bin along the z-axis
    double z_high() const;
    /// Returns the number of bins along the Xaxis
    unsigned int nBinsZ() const;

    /// Get the pointer to the RooDataHist
    std::shared_ptr<RooDataHist> get() const;

    /// Get the pointer to the x-axis variable in RooFit representation
    std::shared_ptr<RooRealVar> x_spectrum() const;
    /// Get the pointer to the y-axis variable in RooFit representation
    std::shared_ptr<RooRealVar> y_spectrum() const;
    /// Get the pointer to the z-axis variable in RooFit representation
    std::shared_ptr<RooRealVar> z_spectrum() const;

    /// Name of the histogram template
    std::string name() const;
    /// Set the name of the template. ATTENTION this will reset *all* RooFit
    /// pointers
    void set_name(const std::string& histo_name);

   private:
    void initialize(const std::string& histo_name);

    /// Pointer holding the input histogram
    std::shared_ptr<TH1> m_histo{nullptr};
    std::string m_template_name{};

    /// RooReal var connected to the dataset
    std::shared_ptr<RooRealVar> m_Roo_x_var{nullptr};
    std::shared_ptr<RooRealVar> m_Roo_y_var{nullptr};
    std::shared_ptr<RooRealVar> m_Roo_z_var{nullptr};
    /// Dataset in RooFit format
    std::shared_ptr<RooDataHist> m_roo_hist{nullptr};
};
#endif