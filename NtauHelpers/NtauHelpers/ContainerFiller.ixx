#ifndef NTAUHELPERS_CONTAINERFILLER_IXX
#define NTAUHELPERS_CONTAINERFILLER_IXX

template <class IFillable, class ProcessThis>
ContainerFiller<IFillable, ProcessThis>::ContainerFiller(
    fill_func filler, std::shared_ptr<IFillable> template_ptr)
    : ISortableByID{}, m_func{filler}, m_ptr{template_ptr} {
    std::string template_name = name();
    if (!template_name.empty()) {
        static const ObjectID fillerID{ObjectID::nextID()};
        ObjectID name_id{template_name};
        ObjectID all_id{{fillerID, name_id}};
        setID(all_id);
    }
}
template <class IFillable, class ProcessThis>
PlotFillInstructionWithRef<IFillable, ProcessThis>
ContainerFiller<IFillable, ProcessThis>::make_instruction(
    const ContainerSelection<ProcessThis>& selection) const {
    PlotFillInstructionWithRef<IFillable, ProcessThis> instruct{
        selection.size(), selection.obj_cut(), get_filler(), get()};

    std::vector<ObjectID> new_ids{};
    for (const auto& sub : getID().allIDs()) {
        new_ids += ObjectID{sub};
    }
    for (const auto& sub : selection.getID().allIDs()) {
        new_ids += ObjectID{sub};
    }
    instruct.setID(ObjectID{new_ids});

    return instruct;
}
#endif
