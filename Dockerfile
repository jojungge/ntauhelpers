FROM atlas/athanalysis:22.2.105

ADD . /project/NtauHelpers
WORKDIR /project/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /project && \
    mv ../NtauHelpers/CMakeLists_CI.txt ../NtauHelpers/CMakeLists.txt && \
    cmake ../NtauHelpers && \
    make -j4

